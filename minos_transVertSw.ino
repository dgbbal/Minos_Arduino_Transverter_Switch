//**************************************************************//
//																//
// This is a simple switcher controller reading data from		//
// Minos Transvert Switch Message.								//
// The content of the messages is an ASCII number that			//
// translates to a switch number.								//
//																//
// Drives an 8 Relay Board, with individual control lines.	    //
//																//
// (C) David Balharrie 2018										//
//																//
//**************************************************************//



byte inByte = 0;  // incoming serial byte
byte serial_buffer[50];  // incoming serial byte buffer
int serial_buffer_index = 0;  // The index pointer variable for the Serial buffer

const char SOM = ':';
const char EOM = '\n';

// ardunino uno relay port numbers
const int RELAY1_PORT = 2;
const int RELAY2_PORT = 3;
const int RELAY3_PORT = 4;
const int RELAY4_PORT = 5;
const int RELAY5_PORT = 6;
const int RELAY6_PORT = 7;
const int RELAY7_PORT = 8;
const int RELAY8_PORT = 9;

const int portNum[] = { RELAY1_PORT, RELAY2_PORT, RELAY3_PORT, RELAY4_PORT, RELAY5_PORT, RELAY6_PORT, RELAY7_PORT, RELAY8_PORT };
const int NUMRELAYS = 8;
const int RELAY_ON = LOW;
const int RELAY_OFF = HIGH;


const char CLEAR_RELAY_CODE = '0';
const int FIRST_RELAY = 1;
const int LAST_RELAY = 8;

void setup()
{


	const long BAUD_RATE = 9600;	// Serial Port communication rate
									
	Serial.begin(BAUD_RATE);


	// set arduino ports as outputs

	for (int i = 0; i < NUMRELAYS; i++)
	{
		pinMode(portNum[i], OUTPUT);
	}

	clearRelays();

}

void loop()
{



	if (Serial.available() > 0) // Get the Serial Data if available
	{
		check_serial();
	}

	

}




void check_serial() // Function to check for data on the Serial port
{

	int code = 0;


	if (Serial.available() > 0) // Get the Serial Data if available
	{


		inByte = Serial.read();  // Get the Serial Data
		if (inByte <= 9 || inByte > 127)  // ignore non ascii data
		{
			return;
		}
		if (inByte != EOM)  // Add to buffer if not LF
		{
			//Serial.print(inByte);
			serial_buffer[serial_buffer_index] = inByte;
			serial_buffer_index++;  // Increment the Serial Buffer pointer

		}
		else
		{  // It's a LF, execute commands
			serial_buffer[serial_buffer_index] = inByte;
			if (serial_buffer[0] == SOM)			// we have a complete message
			{
				if (serial_buffer[1] == CLEAR_RELAY_CODE)
				{
					clearRelays();
				}
				else
				{
					clearRelays();
					int i = 1;
					while (serial_buffer[i] != EOM)
					{
						code = serial_buffer[i] - 48;		// convert ascii to decimal digit
						if (code >= FIRST_RELAY && code <= LAST_RELAY)
						{
							relay_on(code);
						}
						i++;
					}
				}

				serial_buffer_index = 0;  // Clear the Serial Buffer and Reset the Buffer Index Pointer
				serial_buffer[0] = 0;
			}

		}
	}


}


void clearRelays()
{
	//Serial.println("Clear Relays");
	for (int i = 0; i < NUMRELAYS; i++)
	{
		digitalWrite(portNum[i], RELAY_OFF);
	}
}


void relay_on(int relayNum)
{
	//Serial.println("Relay On");
	//Serial.println(relayNum);
	digitalWrite(portNum[relayNum - 1], RELAY_ON);
}