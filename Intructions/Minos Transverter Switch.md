# Minos Transverter Switch

Minos Rigcontrol can be configured to send a switching message when a transverter is selected on a radio. The ascii string message is sent out of the rigcontrol computer comport at the time of selection. A clear switches message is sent when no transverter is selected. 

A micro-controller such as an Arduino Uno can be utilised with a suitable relay board. The Arduino USB port is connected the computer USB.
The computer serial port is configured as 9600 Baud, 8 data, 1 stop, No parity. This is hard coded in Minos. The comport number is defined in the radio configuration tab in Minos.

The message sent is in the form \<‘:’\>\<Message\>\<’\n’\>.
\<Message\> is \<’0\>’ to clear all relays.
\<Message\>  can be any ascii sequence of characters when a transverter is selected, if you are going to write your own software. The values are entered in to the Switch Number box, available for each transverter. If this is left blank, no message will be sent.

While you are free to build and write your own interface, below is an example using an Arduino Uno.

The 5 volt relay board is an example that is available on the internet for a reasonable price, boards with less relays can also be found if 8 aren’t required. Arduino Uno’s are also available for a reasonable price. This document does not take you through loading the software, for which there are many tutorials on the internet to guide you through the process.

![Figure 1 Minos TransVerter Switch Circuit](Images/Minos_Arduino_Transverter_Switch_Circuit.jpg)

Figure 1 shows how to connect the IO ports to the relay board. Power for the relay board and Arduino can be taken from the computer USB port. 

**The capacitor C1 is important, as it prevents the board performing a reset when the comport is initialised on the computer. C1 needs to be out of circuit when uploading the switch control software to the UNO, and in circuit when the software is running.**

Inter-connections between the two boards can be achieved with the male-female Dupont jumper leads that can be purchased on the internet.
The Arduino Sketch can be downloaded from here.

This is an example but will probably suits most users’ requirements. This software will switch relays 1 to 8. If ‘1’ is entered in to the transverter switch number box, relay 1 will be turned on when that transverter is selected.  If more than one number is entered – for example ‘158’, then relay 1, 5 and 8 will be selected. So, it is possible to turn on more than one relay at a transverter selection, if that is required.

![Figure 2 Arduino and Relay Board](Images/Arduino_and_Relay_Board.jpg)


